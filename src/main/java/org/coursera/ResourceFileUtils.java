package org.coursera;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class ResourceFileUtils {
    public static Reader createReaderFromResourceFile(String fileName) throws FileNotFoundException {
        ClassLoader classLoader = ResourceFileUtils.class.getClassLoader();
        InputStream resourceAsStream = classLoader.getResourceAsStream(fileName);
        if (resourceAsStream == null) {
            throw new FileNotFoundException(fileName);
        }
        return new BufferedReader(new InputStreamReader(resourceAsStream));
    }
}
