package org.coursera.entities;

import java.util.Objects;

public class Rating implements Comparable<Rating> {
    private final String item;
    private final double value;

    public Rating(String item, double value) {
        this.item = item;
        this.value = value;
    }

    public String getItem() {
        return item;
    }

    public double getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Rating{" +
                "item='" + item + '\'' +
                ", value=" + value +
                '}';
    }

    public int compareTo(Rating rating) {
        return Double.compare(this.value, rating.value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rating rating = (Rating) o;
        return Double.compare(rating.value, value) == 0 &&
                Objects.equals(item, rating.item);
    }

    @Override
    public int hashCode() {
        return Objects.hash(item, value);
    }
}
