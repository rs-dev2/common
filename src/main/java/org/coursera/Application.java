package org.coursera;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * Hello world!
 */
public class Application {
    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService executorService = Executors.newWorkStealingPool(4);
        List<Callable<String>> callables = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            final int rs = i;
            callables.add(() -> {
                logger.info(Thread.currentThread().getName());
                return String.valueOf(rs);
            });
        }
        List<Future<String>> futures = executorService.invokeAll(callables, 55000, TimeUnit.MILLISECONDS);
        for (Future<String> future : futures) {
            String s = future.get();
            logger.info(s);
        }
        executorService.shutdown();
    }
}
