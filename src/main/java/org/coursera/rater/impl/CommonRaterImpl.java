package org.coursera.rater.impl;

import org.coursera.entities.Rating;
import org.coursera.rater.Rater;

import java.util.ArrayList;
import java.util.List;

public class CommonRaterImpl implements Rater {
    private String id;
    private List<Rating> ratings;

    public CommonRaterImpl(String id) {
        this.id = id;
        this.ratings = new ArrayList<>();
    }

    @Override
    public void addRating(String item, double rating) {
        this.ratings.add(new Rating(item, rating));
    }

    public boolean hasRating(String item) {
        for (Rating rating : ratings) {
            if (rating.getItem().equals(item)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public double getRating(String item) {
        for (Rating rating : ratings) {
            if (rating.getItem().equals(item)) {
                return rating.getValue();
            }
        }

        return -1;
    }

    @Override
    public int numRatings() {
        return ratings.size();
    }

    @Override
    public List<String> getItemsRated() {
        ArrayList<String> list = new ArrayList<>();
        for (Rating rating : ratings) {
            list.add(rating.getItem());
        }

        return list;
    }
}
