package org.coursera.rater;

import java.util.List;

public interface Rater {
    void addRating(String item, double rating);

    String getId();

    double getRating(String item);

    int numRatings();

    List<String> getItemsRated();

    default boolean hasRating(String item){
        return false;
    }
}
